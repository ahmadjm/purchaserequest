import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';

import PurchaseOrderByThirdParty from "./components/purchaseOrder/index";
import { getAuthorizationToken } from './utils/localstorageManipulate';
import { Login } from './components/login/login';

const App = ()=> {
  const[isLogIn,setIsLogin] = useState(false);
  useEffect(() => {
    const token = getAuthorizationToken();
    if(token !== null)
      setIsLogin(true);
  }, []);
  return(
    isLogIn ?
    <PurchaseOrderByThirdParty  setIsLogin={setIsLogin} />
      :
    <Login setIsLogin={setIsLogin} />
  )
 }

export default App;
