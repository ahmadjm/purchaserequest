import axios, { AxiosResponse } from "axios";
import { IApiResponse } from "../models/apiResponse";
import { ILogin } from "../models/users";

axios.defaults.baseURL = "http://185.211.58.28:8080/api/v1";

export const loginApi = (
  username: string,
  password: string
): Promise<AxiosResponse<IApiResponse<ILogin>>> => {
  return axios.post<IApiResponse<ILogin>>("/users/auth", {
    national_id: username,
    password: password,
  });
};

export const createPurchaseApi = (
  purchase: any
): Promise<AxiosResponse<IApiResponse<string>>> => {
  return axios.post("/third-parties/purchase", purchase);
};
