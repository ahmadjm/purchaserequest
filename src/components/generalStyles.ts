import { Components } from "antd/lib/date-picker/generatePicker"
import styled, { css } from "styled-components";

export const Container = styled.div `
    display:flex;
    flex: 1;
    direction :rtl;
    & form {
        margin :10vh auto;
        width : 32vw;
        -webkit-box-shadow: -1px 2px 26px 0px rgba(0,0,0,0.59);
        -moz-box-shadow: -1px 2px 26px 0px rgba(0,0,0,0.59);
        box-shadow: -1px 2px 26px 0px rgba(0,0,0,0.59);
        padding : 40px;
        background-color : #a1bcc9;
    }
    
`