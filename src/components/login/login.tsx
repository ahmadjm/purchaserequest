import React, { useState } from "react";
import { Form, Input, Button, Checkbox } from "antd";
import { Container } from "../generalStyles";
import { LoginLogic } from "./loginLogic";
import { env } from "process";

export const Login = (props:{setIsLogin :(isLogin:boolean)=>void;}) => {
  const {setIsLogin} = props;
  const { login, loading } = LoginLogic(setIsLogin);
  
  return (
    <Container>
      <Form name="basic" onFinish={login}>
        <Form.Item
          label="نام کاربری"
          name="username"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="رمزعبور"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            ورود
          </Button>
        </Form.Item>
      </Form>
    </Container>
  );
};
