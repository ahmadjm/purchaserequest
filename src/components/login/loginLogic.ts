import { message } from "antd";
import React, { useState } from "react";
import { loginApi } from "../../api/api";
import { setAuthorizationToken } from "../../utils/localstorageManipulate";
import { message  as AntMessage } from 'antd';

const info = () => {
    message.info('اطلاعات وارد شده صحیح نمیباشد');
  };

export const LoginLogic = (setIsLogin:(isLogin : boolean)=>void)=>{
    const [loading,setLoading]= useState(false);
    const login = async(values:any)=>{
    try {
        setLoading(true);
        const loginRes = await loginApi(values.username,values.password);
        const token = loginRes.data.result.token;
        if(token !== null){
            setAuthorizationToken(token);
            setIsLogin(true);
        }    
    }
    catch(e){}
    finally{
        setLoading(false);
    }
    }
    return{
        login,
        loading
    }
}