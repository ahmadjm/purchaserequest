import React, { useEffect, useState } from "react";
import { Form, Input, Button, Checkbox, Upload, Space } from "antd";
import { Container } from "../../components/generalStyles";
import { DateTimeInput } from "react-hichestan-datetimepicker";
import { UploadOutlined, InboxOutlined } from "@ant-design/icons";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { removeAuthorizationToken } from "../../utils/localstorageManipulate";
import { createPurchaseApi } from "../../api/api";

function imgToBase64(img: any) {
  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");
  canvas.width = img.width;
  canvas.height = img.height;

  // I think this won't work inside the function from the console
  img.crossOrigin = "anonymous";

  if (ctx !== null) ctx.drawImage(img, 0, 0);

  return canvas.toDataURL();
}
const PurchaseOrderByThirdParty = (props: {
  setIsLogin: (isLogin: boolean) => void;
}) => {
  const [expireDate, setExpireDate] = useState("");
  const handleFromDateChange = (e: any) => {
    setExpireDate(e.target.value);
  };
  const [payablesExpireDates, setPayablesExpireDate] = useState([] as any);
  const handlePayablesExpireDate = (e: any) => {
    setPayablesExpireDate(e.target.value);
  };
  const { setIsLogin } = props;
  const logOut = () => {
    try {
      removeAuthorizationToken();
      setIsLogin(false);
    } catch (e) {}
  };
  const onFinish = (values: any) => {
    console.log(values);
    values = {
      ...values,
      expiresAt: Date.parse(values.expiresAt),
      factors: imgToBase64(values.factors.response),
      payables: values.payables.map((payable: any) =>
        Date.parse(payable.dueDate)
      ),
    };
    console.log(values);
    createPurchaseApi(values);
  };
  return (
    <Container>
      <Button
        danger
        type={"primary"}
        style={{ margin: "1vw" }}
        onClick={logOut}
      >
        خروج{" "}
      </Button>
      <Form name="basic" onFinish={onFinish}>
        <Form.Item
          label="کدمشتری فروشنده"
          name="seller_customer_code"
          rules={[
            { required: true, message: "Please input seller customer code!" },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="کدمشتری خریدار"
          name="buyer_customer_code"
          rules={[
            { required: true, message: "Please input buyer customer code!" },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="تاریخ انقضاء"
          name="expiresAt"
          rules={[
            { required: true, message: "Please choose buyer expiresAt!" },
          ]}
        >
          <DateTimeInput
            onChange={(e: any) => handleFromDateChange(e)}
            name={"from"}
            value={expireDate}
          />
        </Form.Item>

        <Form.Item
          label="تصویر فاکتور"
          name="factors"
          rules={[{ required: true, message: "Please upload your factor!" }]}
        >
          <Upload name="factors" listType="picture">
            <Button icon={<UploadOutlined />}>آپلود</Button>
          </Upload>
        </Form.Item>
        <Form.List name="payables">
          {(fields, { add, remove }) => (
            <>
              {fields.map((field) => (
                <Space
                  key={field.key}
                  style={{ display: "flex", marginBottom: 8 }}
                  align="baseline"
                >
                  <Form.Item
                    {...field}
                    name={[field.name, "price"]}
                    fieldKey={[field.fieldKey, "price"]}
                    rules={[{ required: true, message: "Please enter price" }]}
                  >
                    <Input placeholder="قیمت" />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    name={[field.name, "description"]}
                    fieldKey={[field.fieldKey, "description"]}
                    rules={[
                      { required: true, message: "Please enter description" },
                    ]}
                  >
                    <Input placeholder="توضیحات" />
                  </Form.Item>
                  <Form.Item
                    {...field}
                    name={[field.name, "dueDate"]}
                    fieldKey={[field.fieldKey, "dueDate"]}
                    rules={[
                      { required: true, message: "Please Enter Due Date" },
                    ]}
                  >
                    <DateTimeInput
                      onChange={(e: any) => handleFromDateChange(e)}
                      name={"from"}
                      value={expireDate}
                    />
                  </Form.Item>
                  <MinusCircleOutlined
                    title={"حذف پرداختنی"}
                    onClick={() => remove(field.name)}
                  />
                </Space>
              ))}
              <Button
                title={"اضافه کردن پرداختنی"}
                onClick={() => add()}
                icon={<PlusOutlined />}
                style={{ margin: "20px" }}
              >
                اضافه کردن پرداختنی
              </Button>
            </>
          )}
        </Form.List>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            ارسال درخواست خرید
          </Button>
        </Form.Item>
      </Form>
    </Container>
  );
};

export default PurchaseOrderByThirdParty;
