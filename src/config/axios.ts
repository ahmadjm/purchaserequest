import axios from "axios";
import { getAuthorizationToken } from "../utils/localstorageManipulate"; 
import { message } from "antd";

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (err) => {
    if (err.response) {
      if (err.response.status == 401) {
        message.error("خطا");

      } else if (err.response.data && err.response.data.errors) {
        message.error(err.response.data.errors.join(","));
      }
    } else {
      message.error("ارتباط قطع است");
    }

    return Promise.reject(err);
  }
);

//Add a request interceptor
axios.interceptors.request.use(
  (config) => {
    const token = getAuthorizationToken();
    if (token) {
      config.headers["authorization"] = "Bearer " + token;
    }
    return config;
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error);
  }
);
