export interface IApiResponse<Result> {
  status: number;
  result: Result;
}

export interface IGeneralResponse {
  message: string;
}

