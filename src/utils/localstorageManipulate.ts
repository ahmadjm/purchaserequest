import { LOCALSTORAGE_AUTH_TOKEN_KEY } from "../config/appConfig";

/**
 * Gets authorization token from local storage.
 *
 * @function getAuthorizationToken
 * @returns The local storage authentication key.
 */
export function getAuthorizationToken(): string | null {
  return localStorage.getItem(LOCALSTORAGE_AUTH_TOKEN_KEY);
}

/**
 * Sets authorization token on the local storage.
 *
 * @function setAuthorizationToken
 * @param token {string} token retrieved from the back-end.
 */
export function setAuthorizationToken(token: string) {
  localStorage.setItem(LOCALSTORAGE_AUTH_TOKEN_KEY, token);
}


/**
 * Removes authorization token from the local storage.
 *
 * @function removeAuthorizationToken
 */
export function removeAuthorizationToken() {
  localStorage.removeItem(LOCALSTORAGE_AUTH_TOKEN_KEY);
}
